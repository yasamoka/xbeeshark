XBeeShark v1.00

To test the functionalities provided in XBeeShark, run sample.py.

To include XBeeShark in your own project, place XBeeShark.py in the appropriate directory and import XBeeShark.

XBeeShark requires pySerial and was tested on Python 3.5 and pySerial 3.0.1.