import struct
import time
import binascii
import serial #XBeeShark requires pySerial

def CaptureXBeeFrame(ser):
    while(True):
        start_byte = ser.read(1)
        start_byte1, = struct.unpack("B", start_byte)
        if(start_byte1 == 0x7E):
            break
    length_bytes = ser.read(2)
    length, = struct.unpack("!H", length_bytes)
    frame_bytes = ser.read(length + 1)
    return XBeeFrame(start_byte + length_bytes + frame_bytes)
    
def readline(ser, eol=b'\r'):
    leneol = len(eol)
    line = bytearray()
    while(True):
        c = ser.read(1)
        if(c):
            line += c
            if(line[-leneol:] == eol):
                break
        else:
            break
    return bytes(line[:-leneol])
    
class XBeeATCommandMode:
    PIN_DIO0 = PIN_AD0 = 0
    PIN_DIO1 = PIN_AD1 = 1
    PIN_DIO2 = PIN_AD2 = 2
    PIN_DIO3 = PIN_AD3 = 3
    PIN_DIO4 = 4
    PIN_DIO5 = PIN_ASSOC = 5
    PIN_DIO6 = PIN_RTS = 6
    PIN_DIO7 = PIN_CTS = 7
    PIN_DIO8 = PIN_DTR = PIN_SLEEP_RQ = 8
    PIN_DIO9 = PIN_ON = PIN_SLEEP = 9
    PIN_DIO10 = PIN_PWM0 = 10
    PIN_DIO11 = 11
    PIN_DIO12 = 12
    PIN_DIN = PIN_CONFIG = 13

    PINMODE_DISABLE = 0
    PINMODE_BUILT_IN_FUNCTION = 1
    PINMODE_ANALOG_INPUT = 2
    PINMODE_DIGITAL_INPUT = 3
    PINMODE_DIGITAL_OUTPUT_LOW = 4
    PINMODE_DIGITAL_OUTPUT_HIGH = 5
    PINMODE_RS_485_ENABLE_LOW = 6
    PINMODE_RS_485_ENABLE_HIGH = 7
    
    PERIODIC_SAMPLING_OFF = 0
    SAMPLING_RATE_MIN = 0x32
    SAMPLING_RATE_MAX = 0xFFFF
    
    PULL_UP_RESISTOR_STATE_ALL_OFF = 0
    PULL_UP_RESISTOR_STATE_ALL_ON = 0x3FFF
    PULL_UP_RESISTOR_STATE_OFF = 0
    PULL_UP_RESISTOR_STATE_ON = 1

    def __init__(self, ser):
        self.ser = ser
        if(not self.Attention()):
            self.Enter()
    
    def _get(self, cmd):
        self.ser.write(cmd)
        self.ser.flush()
        line = readline(self.ser)
        #if(line == b'0'):
            #return bytes(0)
        if(len(line) % 2 == 0):
            line1 = b''
        else:
            line1 = b'0'
        line1 += line
        line2 = binascii.unhexlify(line1)
        return line2
    
    def _set(self, cmd):
        self.ser.write(cmd)
        self.ser.flush()
        line = readline(self.ser)
        return line == b'OK\r'
        
    def _getAddress(self, cmd):
        reply = self._get(cmd)
        addr = int.from_bytes(reply, byteorder='big', signed=False)
        return addr
    
    def _getPullUpResistorBitShiftCounter(pin):
        if(pin == XBeeATCommandMode.PIN_CTS):
            bitShiftCounter = 13
        elif(pin == XBeeATCommandMode.PIN_DIO11):
            bitShiftCounter = 12
        elif(pin == XBeeATCommandMode.PIN_DIO10):
            bitShiftCounter = 11
        elif(pin == XBeeATCommandMode.PIN_DIO12):
            bitShiftCounter = 10
        elif(pin == XBeeATCommandMode.PIN_ON):
            bitShiftCounter = 9
        elif(pin == XBeeATCommandMode.PIN_DIO5):
            bitShiftCounter = 8
        elif(pin == XBeeATCommandMode.PIN_DIN):
            bitShiftCounter = 7
        elif(pin == XBeeATCommandMode.PIN_SLEEP_RQ):
            bitShiftCounter = 6
        elif(pin == XBeeATCommandMode.PIN_RTS):
            bitShiftCounter = 5
        elif(pin == XBeeATCommandMode.PIN_DIO0):
            bitShiftCounter = 4
        elif(pin == XBeeATCommandMode.PIN_DIO1):
            bitShiftCounter = 3
        elif(pin == XBeeATCommandMode.PIN_DIO2):
            bitShiftCounter = 2
        elif(pin == XBeeATCommandMode.PIN_DIO3):
            bitShiftCounter = 1
        elif(pin == XBeeATCommandMode.PIN_DIO4):
            bitShiftCounter = 0
        return bitShiftCounter
    
    def Attention(self):
        cmd = b'AT\r'
        return self._set(cmd)
    
    def Enter(self):
        cmd = b'+++'
        return self._set(cmd)
    
    def Drop(self):
        cmd = b'ATCN\r'
        return self._set(cmd)
    
    def Reset(self):
        cmd = b'ATRE\r'
        return self._set(cmd)
    
    def Write(self):
        cmd = b'ATWR\r'
        return self._set(cmd)
    
    def getPANID(self):
        cmd = b'ATID\r'
        reply = self._get(cmd)
        #panid, = struct.unpack("!H", reply)
        panid = int.from_bytes(reply, byteorder='big', signed=False)
        return panid
    
    def getSourceAddressHigher(self):
        return self._getAddress(b'ATSH\r')
    
    def getSourceAddressLower(self):
        return self._getAddress(b'ATSL\r')
    
    def getSourceAddress(self):
        return (self.getSourceAddressHigher() << 32) + self.getSourceAddressLower()
    
    def getDestAddressHigher(self):
        return self._getAddress(b'ATDH\r')
    
    def getDestAddressLower(self):
        return self._getAddress(b'ATDL\r')
    
    def getDestAddress(self):
        return (self.getDestAddressHigher() << 32) + self.getDestAddressLower()
    
    def getSourceNetAddress(self):
        cmd = b'ATMY\r'
        reply = self._get(cmd)
        #src_net_addr, = struct.unpack("!H", reply)
        src_net_addr = int.from_bytes(reply, byteorder='big', signed=False)
        return src_net_addr
    
    def getIOPinConfiguration(self, pin):
        if(pin >= 0 and pin <= 5):
            group = True
        elif(pin >= 10 and pin <= 12):
            group = False
        else:
            return None #throw invalid I/O pin exception
        
        if(group):
            cmd = b'ATD' + b'%i' % pin
        else:
            cmd = b'ATP' + b'%i' % (pin - 10)
        cmd += b'\r'
        reply = self._get(cmd)
        pinmode, = struct.unpack("!B", reply)
        return pinmode
    
    def getIOSamplingRate(self):
        cmd = b'ATIR\r'
        reply = self._get(cmd)
        #IOSamplingRate, = struct.unpack("!H", reply)
        IOSamplingRate = int.from_bytes(reply, byteorder='big', signed=False)
        return IOSamplingRate
    
    def getVoltage(self):
        cmd = b'AT%V\r'
        reply = self._get(cmd)
        #voltage_vcc, = struct.unpack("!H", reply)
        voltage_vcc = int.from_bytes(reply, byteorder='big', signed=False)
        voltage_mV = voltage_vcc * (1200 / 1024)
        return voltage_mV
    
    def getPullUpResistorStateMask(self):
        cmd = b'ATPR\r'
        reply = self._get(cmd)
        #state_mask, = struct.unpack("!H", reply)
        state_mask = int.from_bytes(reply, byteorder='big', signed=False)
        return state_mask
    
    def getPullUpResistorState(self, pin):
        if(pin < XBeeATCommandMode.PIN_DIO0 or pin > XBeeATCommandMode.PIN_DIN):
            return None #throw invalid I/O pin exception
        bitShiftCounter = XBeeATCommandMode._getPullUpResistorBitShiftCounter(pin)
        state_mask = self.getPullUpResistorStateMask()
        state = (state_mask >> bitShiftCounter) & 0b1
        return state
    
    def setPANID(self, panid):
        panid_bytes = struct.pack("!H", panid)
        panid_hex = binascii.hexlify(panid_bytes)
        cmd = b'ATID ' + panid_hex + b'\r'
        return self._set(cmd)
    
    def setDestAddressHigher(self, dst_addr_H):
        dst_addr_H_bytes = struct.pack("!I", dst_addr_H)
        dst_addr_H_hex = binascii.hexlify(dst_addr_H_bytes)
        cmd = b'ATDH ' + dst_addr_H_hex + b'\r'
        return self._set(cmd)
    
    def setDestAddressLower(self, dst_addr_L):
        dst_addr_L_bytes = struct.pack("!I", dst_addr_L)
        dst_addr_L_hex = binascii.hexlify(dst_addr_L_bytes)
        cmd = b'ATDL ' + dst_addr_L_hex + b'\r'
        return self._set(cmd)
    
    def setDestAddress(self, dst_addr):
        dst_addr_H = dst_addr >> 32
        dst_addr_L = dst_addr & 0xFFFFFFFF
        high = self.setDestAddressHigher(dst_addr_H)
        low = self.setDestAddressLower(dst_addr_L)
        return high and low
    
    def setIOPinConfiguration(self, pin, pinmode):
        if(pin >= 0 and pin <= 7):
            group = True
        elif(pin >= 10 and pin <= 12):
            group = False
        else:
            return None #throw invalid I/O pin exception
        if(pinmode < XBeeATCommandMode.PINMODE_DISABLE or pinmode > XBeeATCommandMode.PINMODE_RS_485_ENABLE_HIGH):
            return None #throw invalid pinmode exception
        if(pinmode == XBeeATCommandMode.PINMODE_ANALOG_INPUT and pin > XBeeATCommandMode.PIN_AD3):
            return None #throw unsupported pinmode exception
        if((pinmode == XBeeATCommandMode.PINMODE_RS_485_ENABLE_LOW or pinmode == XBeeATCommandMode.PINMODE_RS_485_ENABLE_HIGH) and pin != XBeeATCommandMode.PIN_CTS):
            return None #throw unsupported pinmode exception
        if(pinmode == XBeeATCommandMode.PINMODE_BUILT_IN_FUNCTION and not (pin == XBeeATCommandMode.PIN_DIO0 or pin == XBeeATCommandMode.PIN_ASSOC or pin == XBeeATCommandMode.PIN_PWM0 or pin == XBeeATCommandMode.PIN_RTS or pin == XBeeATCommandMode.PIN_CTS)):
            return None #throw unsupported pinmode exception
        
        if(group):
            cmd = b'ATD' + b'%i' % pin
        else:
            cmd = b'ATP' + b'%i' % (pin - 10)
        cmd += b' ' + b'%i' % pinmode + b'\r'
        return self._set(cmd)
    
    def setIOSamplingRate(self, sampling_rate):
        if((sampling_rate != XBeeATCommandMode.PERIODIC_SAMPLING_OFF and sampling_rate < XBeeATCommandMode.SAMPLING_RATE_MIN) or sampling_rate > XBeeATCommandMode.SAMPLING_RATE_MAX):
            return None #throw invalid I/O sampling rate value exception
        sampling_rate_bytes = struct.pack("!H", sampling_rate)
        sampling_rate_hex = binascii.hexlify(sampling_rate_bytes)
        cmd = b'ATIR ' + sampling_rate_hex + b'\r'
        return self._set(cmd)
    
    def setPullUpResistorStateMask(self, state_mask):
        if(state_mask < XBeeATCommandMode.PULL_UP_RESISTOR_STATE_ALL_OFF or state_mask > XBeeATCommandMode.PULL_UP_RESISTOR_STATE_ALL_ON):
            return None #throw invalid pull up resistor state mask value exception
        state_mask_bytes = struct.pack("!H", state_mask)
        state_mask_hex = binascii.hexlify(state_mask_bytes)
        cmd = b'ATPR ' + state_mask_hex + b'\r'
        return self._set(cmd)
    
    def setPullUpResistorState(self, pin, state):
        if(pin < XBeeATCommandMode.PIN_DIO0 or pin > XBeeATCommandMode.PIN_DIN):
            return None #throw invalid I/O pin exception
        if(not (state == XBeeATCommandMode.PULL_UP_RESISTOR_STATE_OFF or state == XBeeATCommandMode.PULL_UP_RESISTOR_STATE_ON)):
            return None #throw invalid pull up resistor state value exception
        
        bitShiftCounter = XBeeATCommandMode._getPullUpResistorBitShiftCounter(pin)
        state_mask = self.getPullUpResistorStateMask()
        state_mask1 = state_mask & (XBeeATCommandMode.PULL_UP_RESISTOR_STATE_ALL_ON - (0b1 << bitShiftCounter))
        state_mask1 += (state << bitShiftCounter)
        self.setPullUpResistorStateMask(state_mask1)


class XBeeFrame:

    FORMAT_HEX = "0x"
    FORMAT_READABLE = "r"

    def __init__(self, frame):
        frame1 = frame
        bytes = struct.unpack('!BHBQHBBHB', frame1[0:19])
        frame1 = frame1[19:]
        
        self.start_byte = bytes[0]
        self.length = bytes[1]
        self.frame_type = bytes[2]
        self.src_addr = bytes[3]
        self.src_net_addr = bytes[4]
        self.recv_opts = bytes[5]
        self.num_of_sample_sets = bytes[6]
        self.digital_channel_mask = bytes[7]
        self.analog_channel_mask = bytes[8]
        
        self.digital_sample_data = None
        if(self.digital_channel_mask):
            bytes_digital = struct.unpack("!H", frame1[0:2])
            frame1 = frame1[2:]
            self.digital_sample_data = bytes_digital[0]
        
        self.analog_sample_data = []
        if(self.analog_channel_mask):
            for i in range(0, bin(self.analog_channel_mask).count("1")):
                bytes_analog = struct.unpack("!H", frame1[0:2])
                frame1 = frame1[2:]
                self.analog_sample_data.append(bytes_analog[0])
        
        bytes_checksum = struct.unpack("!B", frame1)
        self.checksum = bytes_checksum[0]
        
        self.checksum_calc = 0xFF - (sum(frame[3:3+self.length]) & 0xFF)
    
    def verify(self):
        return self.start_byte == 0x7e and self.checksum == self.checksum_calc
    
    def getLength(self):
        return self.length
    
    def getFrameType(self):
        return self.frame_type
    
    def getSourceAddress(self):
        return self.src_addr
    
    def getSourceNetAddress(self):
        return self.src_net_addr
    
    def getReceiveOpts(self):
        return self.recv_opts
    
    def getAllEnabledDIOPins(self): #returns a list of enabled Digital IO pins
        if(self.digital_channel_mask == 0):
            return None
        enabledDIOPins = []
        DIOPinMask = 0x0001
        for i in range(0, 8):
            if(self.digital_channel_mask & DIOPinMask):
                enabledDIOPins.append(i)
            DIOPinMask = DIOPinMask << 1
        DIOPinMask = DIOPinMask << 1
        for j in range(10, 13):
            DIOPinMask = DIOPinMask << 1
            if(self.digital_channel_mask & DIOPinMask):
                enabledDIOPins.append(j)
        return enabledDIOPins
    
    #returns a list of the states of all Digital IO pins
    def getAllDIOPinStates(self):
        enabledDIOPins = [False, False, False, False, False, False, False, False, None, None, False, False, False]
        if(self.digital_channel_mask == 0):
            return enabledDIOPins
        DIOPinMask = 0x0001
        for i in range(0, 8):
            if(self.digital_channel_mask & DIOPinMask):
                enabledDIOPins[i] = True
            DIOPinMask = DIOPinMask << 1
        DIOPinMask = DIOPinMask << 1
        for j in range(10, 13):
            DIOPinMask = DIOPinMask << 1
            if(self.digital_channel_mask & DIOPinMask):
                enabledDIOPins[j] = True
        return enabledDIOPins
    
    #returns the state of the specified Digital IO pin
    def isDIOPinEnabled(self, pin):
        #throw exception for invalid pin
        DIOPinMask = 0x0001 << pin
        return self.digital_channel_mask & DIOPinMask != 0
    
    def getDIOPinReading(self, pin):
        if(not self.isDIOPinEnabled(pin)):
            return None
        DIOPinMask = 0x0001 << pin
        return self.digital_sample_data & DIOPinMask != 0
    
    #returns a list of enabled Analog IO pins
    def getAllEnabledADPins(self):
        if(self.analog_channel_mask == 0):
            return None
        enabledADPins = []
        ADPinMask = 0x01
        for i in range(0, 4):
            if(self.analog_channel_mask & ADPinMask):
                enabledADPins.append(i)
            ADPinMask = ADPinMask << 1
        return enabledADPins
    
    #returns a list of the states of all Analog IO pins
    def getAllADPinStates(self):
        enabledADPins = [False] * 4
        if(self.analog_channel_mask == 0):
            return enabledADPins
        ADPinMask = 0x01
        for i in range(0, 4):
            if(self.analog_channel_mask & ADPinMask):
                enabledADPins[i] = True
            ADPinMask = ADPinMask << 1
        return enabledADPins
    
    #returns the state of the specified Analog IO pin
    def isADPinEnabled(self, pin):
        #throw exception for invalid pin
        ADPinMask = 0x0001
        ADPinMask = ADPinMask << pin
        return self.analog_channel_mask & ADPinMask != 0
    
    def getADPinReading(self, pin):
        if(self.isADPinEnabled(pin) == False):
            return None
        pinIndex = 0
        analogDataSampleIndex = False
        for enabledADPin in self.getAllEnabledADPins():
            if(enabledADPin == pin):
                pinFound = True
                break
            analogDataSampleIndex = analogDataSampleIndex + 1
        if(pinFound == False):
            return None
        return self.analog_sample_data[analogDataSampleIndex]
    
    def __format__(self, fmt):
        if(fmt == self.FORMAT_HEX):
            return self.formatHex()
        elif(fmt == self.FORMAT_READABLE):
            return self.formatReadable()
        else:
            return None #raise exception instead
    
    def __str__(self):
        return self.formatReadable()
    
    def formatHex(self):
        s = ""
        s += "Start Byte:\t\t0x" + format(self.start_byte, '02X') + '\n'
        s += "Length:\t\t\t0x" + format(self.length, '02X') + '\n'
        s += "Frame Type:\t\t0x" + format(self.frame_type, '02X') + '\n'
        s += "Source Address (S/N):\t0x" + format(self.src_addr, '016X') + '\n'
        s += "Source Net Address:\t0x" + format(self.src_net_addr, '04X') + '\n'
        s += "Receive Options:\t0x" + format(self.recv_opts, '02X') + '\n'
        s += "Number of Sample Sets:\t0x" + format(self.num_of_sample_sets, '02X') + '\n'
        s += "Digital Channel Mask:\t0x" + format(self.digital_channel_mask, '04X') + '\n'
        s += "Analog Channel Mask:\t0x" + format(self.analog_channel_mask, '02X') + '\n'
        
        if(self.digital_channel_mask):
            s += "Digital Sample Data:\t0x" + format(self.digital_sample_data, '04X') + '\n'
        
        if(self.analog_channel_mask):
            s += "Analog Sample Data:\t0x" + format(self.analog_sample_data[0], '04X')
            for i in range(1, bin(self.analog_channel_mask).count("1")):
                s += " 0x" + format(self.analog_sample_data[i], '04X')
            s += '\n'
        s += "Checksum:\t\t0x" + format(self.checksum, '02X') + '\n'
        return s
        
    def formatReadable(self):
        s = ""
        s += "Start Byte:\t\t0x" + format(self.start_byte, '02X') + '\n'
        s += "Length:\t\t\t" + str(self.length) + '\n'
        s += "Frame Type:\t\t0x" + format(self.frame_type, '02X') + " ("
        
        if(self.frame_type == 0x17):
            s += "AT Command"
        elif(self.frame_type == 0x92):
            s += "Data Sample"
        else:
            s += "Unknown"
        s += ")" + '\n'
        
        s += "Source Address (S/N):\t" + format(self.src_addr, '016X') + '\n'
        s += "Source Net Address:\t" + format(self.src_net_addr, '04X') + '\n'
        
        s += "Receive Options:\t0x" + format(self.recv_opts, '02X') + '\n'
        if(self.recv_opts & 0x01):
            s += "\t\t\t0x01 Packet Acknowledged" + '\n'
        if(self.recv_opts & 0x02):
            s += "\t\t\t0x02 Broadcast Packet" + '\n'
        
        s += "Number of Sample Sets:\t" + str(self.num_of_sample_sets) + '\n'
        s += "Digital Channel Mask:\t0x" + format(self.digital_channel_mask, '04X') + '\n'
        s += "Analog Channel Mask:\t0x" + format(self.analog_channel_mask, '02X') + '\n'
        
        if(self.digital_channel_mask):
            s += "Digital Sample Data:\t0x" + format(self.digital_sample_data, '04X') +'\n'
            for enabledDIOPin in self.getAllEnabledDIOPins():
                val = self.digital_sample_data & (0x0001 << enabledDIOPin) != 0
                s += "\tDIO" + str(enabledDIOPin) + ":\t\t" + str(val) + '\n'
        
        if(self.analog_channel_mask):
            s += "Analog Sample Data:" + '\n'
            analogDataSampleIndex = 0
            for enabledADPin in self.getAllEnabledADPins():
                s += "\tAD" + str(enabledADPin) + ":\t\t0x" + format(self.analog_sample_data[analogDataSampleIndex], '04X') + '\n'
                analogDataSampleIndex += 1
        
        s += "Checksum: \t\t0x" + format(self.checksum, '02X') + '\n'
        return s