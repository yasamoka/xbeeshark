import struct
import serial
from XBeeShark import XBeeATCommandMode
import binascii
import codecs
import time

print("Starting serial interface...")
ser = serial.Serial('COM4', 9600, timeout=1)
print("Entering AT Command Mode...")
ATCommandMode = XBeeATCommandMode(ser)

AT = ATCommandMode.Attention()
PANID = ATCommandMode.getPANID()
srcAddr = ATCommandMode.getSourceAddress()
dstAddr = ATCommandMode.getDestAddress()
srcNetAddr = ATCommandMode.getSourceNetAddress()
pin0Config = ATCommandMode.getIOPinConfiguration(XBeeATCommandMode.PIN_AD0)
pin3Config = ATCommandMode.getIOPinConfiguration(XBeeATCommandMode.PIN_DIO3)
pin12Config = ATCommandMode.getIOPinConfiguration(XBeeATCommandMode.PIN_DIO12)
IOSamplingRate = ATCommandMode.getIOSamplingRate()
voltage = ATCommandMode.getVoltage()
pullUpResistorStateMask = ATCommandMode.getPullUpResistorStateMask()
pin0PullUpResistorState = ATCommandMode.getPullUpResistorState(XBeeATCommandMode.PIN_AD0)
pin3PullUpResistorState = ATCommandMode.getPullUpResistorState(XBeeATCommandMode.PIN_AD3)
pin12PullUpResistorState = ATCommandMode.getPullUpResistorState(XBeeATCommandMode.PIN_DIO12)

print("Attention: " + str(AT))
print("PAN ID : 0x" + format(PANID, '04X'))
print("Source Address: 0x" + format(srcAddr, '016X'))
print("Destination Address: 0x" + format(dstAddr, '016X'))
print("Source Network Address: 0x" + format(srcNetAddr, '04X'))
print("Pin 0 Configuration: " + str(pin0Config))
print("Pin 3 Configuration: " + str(pin3Config))
print("Pin 12 Configuration: " + str(pin12Config))
print("IO Sampling Rate: 0x" + format(IOSamplingRate, '04X'))
print("Voltage: " + str(voltage) + " mV")
print("Pull-Up Resistor State Mask: 0x" + format(pullUpResistorStateMask, '04X'))
print("Pin 0 Pull-Up Resistor State: " + str(pin0PullUpResistorState))
print("Pin 3 Pull-Up Resistor State: " + str(pin3PullUpResistorState))
print("Pin 12 Pull-Up Resistor State: " + str(pin12PullUpResistorState))
print()

print("Resetting...")
ATCommandMode.Reset()

panid = 0x2000
dst_addr = 0x001122334455667A

pin0 = XBeeATCommandMode.PIN_AD0
pinmode0 = XBeeATCommandMode.PINMODE_ANALOG_INPUT

pin3 = XBeeATCommandMode.PIN_DIO3
pinmode3 = XBeeATCommandMode.PINMODE_DIGITAL_INPUT
state3 = XBeeATCommandMode.PULL_UP_RESISTOR_STATE_OFF

pin12 = XBeeATCommandMode.PIN_DIO12
pinmode12 = XBeeATCommandMode.PINMODE_DIGITAL_OUTPUT_LOW

sampling_rate = XBeeATCommandMode.SAMPLING_RATE_MIN
state_mask = 0x3FFF

ATCommandMode.setPANID(panid)
ATCommandMode.setDestAddress(dst_addr)
ATCommandMode.setIOPinConfiguration(pin0, pinmode0)
ATCommandMode.setIOPinConfiguration(pin3, pinmode3)
ATCommandMode.setIOPinConfiguration(pin12, pinmode12)

ATCommandMode.setIOSamplingRate(sampling_rate)
ATCommandMode.setPullUpResistorStateMask(state_mask)
ATCommandMode.setPullUpResistorState(pin3, state3)

print("Writing...")
ATCommandMode.Write()
print("Dropping from AT Command Mode...")
ATCommandMode.Drop()