import struct
import serial
from XBeeShark import XBeeFrame
from XBeeShark import CaptureXBeeFrame

frame_sample_bytes = bytes([0x7E, 0x00, 0x16, 0x92, 0x00, 0x13, 0xA2, 0x00, 0x40, 0xDC, 0x80, 0x4C, 0xB9, 0xCE, 0x01, 0x01, 0x00, 0x30, 0x0A, 0x00, 0x10, 0x40, 0xB2, 0x05, 0xAA, 0x5D])
frame_sample = XBeeFrame(frame_sample_bytes)

print("Printing sample user-generated frame: ")
print(frame_sample)

print("Starting serial interface...")
print()
ser = serial.Serial('COM4', 9600)
frames = []
print("Capturing 4 XBee frames...")
for i in range(0, 4):
    frame = CaptureXBeeFrame(ser)
    frames.append(frame)

print("Printing frame 0 in hexadecimal format...")
print(format(frames[0], XBeeFrame.FORMAT_HEX))
print("Printing frame 1 in human-readable format...")
print(format(frames[1], XBeeFrame.FORMAT_READABLE))

print("Frame 2:")
print("Healthy frame: " + str(frames[2].verify()))
print()

allEnabledDIOPins = frames[2].getAllEnabledDIOPins()
allDIOPinStates = frames[2].getAllDIOPinStates()
DIO4State = frames[2].isDIOPinEnabled(4)
DIO4Reading = frames[2].getDIOPinReading(4)

print("List of enabled DIO pins: " + str(allEnabledDIOPins))
print("List of states of all DIO pins: " + str(allDIOPinStates))
print("DIO4 state: " + str(DIO4State))
print("DIO4 reading: " + str(DIO4Reading))
print()

allEnabledADPins = frames[2].getAllEnabledADPins()
allADPinStates = frames[2].getAllADPinStates()
AD1State = frames[2].isADPinEnabled(1)
AD1Reading = frames[2].getADPinReading(1)

print("List of enabled AD pins: " + str(allEnabledADPins))
print("List of states of all AD pins: " + str(allADPinStates))
print("AD1 state: " + str(AD1State))
print("AD1 reading: " + str(AD1Reading))
print()

length = frames[3].getLength()
frameType = frames[3].getFrameType()
srcAddr = frames[3].getSourceAddress()
srcNetAddr = frames[3].getSourceNetAddress()
recv_opts = frames[3].getReceiveOpts()

print("Frame 3:")
print("Length: " + str(length))
print("Frame type: " + str(frameType))
print("Source Address: " + str(srcAddr))
print("Source Network Address: " + str(srcNetAddr))
print("Receive Options: " + str(recv_opts))